<?php

namespace Pon\LaravelSms\Handler;

use Illuminate\Support\Facades\Redis;
use Pon\LaravelSms\Exceptions\CacheException;

class SmsCache
{
    protected int $interval;
    protected string $sceneId;
    protected string $mobile;

    protected int $expires;
    protected int $mobileSmsDailyMax;
    protected string $cacheKeyPrefix;

    public function __construct($options = [])
    {
        $options = array_merge(config('sms'), $options);
        $this->setExpires($options['expires'] ?? 3);
        $this->setInterval($options['interval'] ?? 60);
        $this->setMobileSmsDailyMax($options['mobile_sms_daily_max'] ?? 0);
        $this->setCacheKeyPrefix($options['cache_key_prefix'] ?? 'sms');
    }

    public function setCacheKeyPrefix(string $prefix): static
    {
        $this->cacheKeyPrefix = $prefix;

        return $this;
    }

    public function getCacheKeyPrefix(): string
    {
        return $this->cacheKeyPrefix;
    }

    /**
     * 设置场景ID
     * @param string $sceneId
     * @return $this
     */
    public function setSceneId(string $sceneId): static
    {
        $this->sceneId = $sceneId;

        return $this;
    }

    public function getSceneId(): string
    {
        if (empty($this->sceneId)) {
            return 'default';
        }

        return $this->sceneId;
    }


    /**
     * 设置有效时长（分钟）
     * @param int $minutes
     * @return $this
     */
    public function setExpires(int $minutes): static
    {
        $this->expires = $minutes;
        return $this;
    }

    public function getExpires(): int
    {
        return $this->expires;
    }

    /**
     * 设置间隔时长（秒）
     * @param int $seconds
     * @return $this
     */
    public function setInterval(int $seconds): static
    {
        $this->interval = $seconds;
        return $this;
    }


    public function getInterval(): int
    {
        return $this->interval;
    }

    /**
     * 设置手机号码
     * @param string $mobile
     * @return $this
     */
    public function setMobile(string $mobile): static
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @throws CacheException
     */
    public function getMobile(): string
    {
        if (empty($this->mobile)) {
            throw new CacheException('未设置手机号');
        }

        return $this->mobile;
    }

    /**
     * 单一手机号每日发送上限
     * @param $mobileSmsDailyMax
     * @return $this
     */
    public function setMobileSmsDailyMax($mobileSmsDailyMax): static
    {
        $this->mobileSmsDailyMax = $mobileSmsDailyMax;
        return $this;
    }

    public function getMobileSmsDailyMax(): int
    {
        return $this->mobileSmsDailyMax;
    }

    /**
     * @throws CacheException
     */
    public function cacheCaptcha(int $captcha)
    {
        if ($this->getInterval() > 0) {
            Redis::setex($this->getKey() . ':interval_lock', $this->getInterval(), $captcha);
        }

        return Redis::setex($this->getKey(), $this->getExpires() * 60, $captcha);
    }

    /**
     * @throws CacheException
     */
    public function validateInterval(): int
    {
        $ttl = Redis::ttl($this->getKey() . ':interval_lock');
        if ($this->getInterval() > 0 && $ttl !== -2) {
            return $ttl;
        }

        return 0;
    }

    /**
     * 单一手机号每日发送上限
     */
    public function validateMobileSmsDailyMax(): bool
    {
        if ($this->getMobileSmsDailyMax() <= 0) {
            return true;
        }

        $sendKey = $this->getKey() . ':' . date('Ymd') . ':send';
        $sendCount = Redis::incr($sendKey);
        if ($sendCount == 1) {
            Redis::expire($sendKey, $this->nowTillTomorrowMorningLeftSenconds());
        }

        if ($sendCount > $this->getMobileSmsDailyMax()) {
            return false;
        }

        return true;
    }

    /**
     * @desc 验证验证码是否正确
     * @param int $captcha
     * @return bool
     * @throws CacheException
     */
    public function validateCaptcha(int $captcha): bool
    {
        if (Redis::get($this->getKey()) == $captcha) {
            Redis::del($this->getKey());
            return true;
        }

        return false;
    }

    /**
     * 现在到明天0点的秒数
     * @return int
     */
    public function nowTillTomorrowMorningLeftSenconds(): int
    {
        return strtotime('+1 day', strtotime(date('Y-m-d'))) - time();
    }

    /**
     * 获取验证码redis存储的key
     * @return string
     * @throws CacheException
     */
    private function getKey(): string
    {
        return $this->getCacheKeyPrefix() . $this->getMobile() . ':' . $this->getSceneId() . ':captcha';
    }
}
